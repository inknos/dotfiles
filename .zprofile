export PATH=$PATH:$HOME/.local/bin
export TERM='konsole'

# Auto Load Pyenv
#export PYENV_ROOT="$HOME/.pyenv"
#export PATH="$PYENV_ROOT/bin:$PATH"
#eval "$(pyenv init -)"

alias kdewayland='XDG_SESSION_TYPE=wayland dbus-run-session startplasmacompositor'
